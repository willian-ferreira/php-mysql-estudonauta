<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Listagem de Jogos</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="estilos/estilo.css"/>
</head>
<body>
    <?php
		require_once 'includes/banco.php';
		require_once 'includes/funcoes.php';
	?>
	<div id="corpo">
    <?php 
        include_once "topo.php";
        $codigo = $_GET['cod'] ?? 0;
        $busca = $banco->query("SELECT * FROM jogos WHERE cod='$codigo'")
    ?>
        <h1>Detalhes do jogo</h1>
        <table class="detalhes">
        <?php 
            if (!$busca) {
                echo "Busca Falhou! $banco->error";
            } else {
                if($busca->num_rows == 1) {
                    $reg = $busca->fetch_object();
                    $t = thumb($reg->capa);
                    echo "<tr>";
                        echo "<td rowspan='3'><img src='$t' class='full'></td>";
                        echo "<td><h2>$reg->nome</h2>";
                        echo "Nota: ". number_format($reg->nota, 1). "/10.0";
                        echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td>$reg->descricao</td>";
                    echo "</tr>";    
                    echo "<tr>";
                        echo "<td>Adm</adm>";    
                    echo "</tr>";
                } else {
                    echo "<tr>";
                        echo "<td>Nenhum registro encontrado";
                    echo "</tr>";    
                }
            }
            ?> 
        </table>
        <a href="index.php"><img src="icones/icoback.png" alt="Icone para voltar"/></a>
    </div>
    <?php include_once "rodape.php"; ?>
</body>
</html>