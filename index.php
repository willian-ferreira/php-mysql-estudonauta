<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Listagem de Jogos</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="estilos/estilo.css"/>
</head>
<body>
	<?php 
		// include = se não encontra o banco continua execução
		// require = se der algum erro ele para execução
		// require_onde = caso importar duas vezes, ele vai importar uma única
		require_once 'includes/banco.php';
		require_once 'includes/funcoes.php';
		$ordem = $_GET['o'] ?? "n";
		$chave = $_GET['c'] ?? "";
	?>
	<div id="corpo">
	<?php include_once "topo.php"; ?>
		<h1>Escolha seu jogo</h1>
		<form method="get" action="index.php" class="busca">
		Ordenar: 
		<a href="index.php?o=n&c=<?php echo $chave; ?>">Nome</a> | 
		<a href="index.php?o=p&c=<?php echo $chave; ?>">Produtora</a> | 
		<a href="index.php?o=n1&c=<?php echo $chave; ?>">Nota Alta</a> | 
		<a href="index.php?o=n2&c=<?php echo $chave; ?>">Nota baixa</a> |
		<a href="index.php">Mostrar Todos</a> |
		Buscar: <input type="text" name="c" size="10" maxlength="40">
		<input type="submit" value="Ok"/>
		</form>
		<table class="listagem">
		<?php
			$query = "SELECT j.cod, j.nome, g.genero, j.capa, p.produtora 
			FROM jogos j 
			join generos g on j.genero = g.cod
			join produtoras p on j.produtora = p.cod ";

			if (!empty($chave)) {
				$query .= "WHERE j.nome like '%$chave%' 
				OR p.produtora like '%$chave%' 
				OR g.genero like '%$chave%' ";
			}

			switch ($ordem) {
				case 'p':
					$query .= "ORDER BY p.produtora";
					break;
				case 'n1':
					$query .= "ORDER BY j.nota DESC";
					break;
				case 'n2':
					$query .= "ORDER BY j.nota ASC";
					break;
				default:
					$query .= "ORDER BY j.nome";
					break;
			}
			$busca = $banco->query($query);
			if (!$busca) {
				echo "<tr><td>Infelizmente a busca deu errado $banco->error";
			} else {
				if ($busca->num_rows == 0) {
					echo "<tr><td>Nenhum registro encontrado";
				} else {
					while($reg = $busca->fetch_object()) {
						$t = thumb($reg->capa);
						echo "<tr>";
						echo "<td>";
						echo "<img src='$t' class='mini'/>";
						echo "</td>";
						echo "<td><a href='detalhes.php?cod=$reg->cod'>$reg->nome</a>";
						echo " [$reg->genero] <br>";
						echo "$reg->produtora";
						echo "</td>";
						echo "<td>Adm</td>";
					}
				}
			}
			?>
		</table>
	</div>
	<?php include_once "rodape.php"; ?>
</body>
</html> 